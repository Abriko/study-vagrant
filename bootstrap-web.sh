#!/bin/bash
echo "run bootstrap-web.sh"

# nginx and php
setenforce 0
sed -i s'/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
yum install -y epel-release yum-utils
yum install -y nginx
yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager --enable remi-php73
yum --enablerepo=remi,remi-php73 install -y php-fpm php-common php-xml php-opcache php-pdo php-mbstring php-mysqlnd php-mcrypt

sed -i s'/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm.sock/' /etc/php-fpm.d/www.conf
sed -i s'/;listen.owner = nobody/listen.owner = nginx/' /etc/php-fpm.d/www.conf
sed -i s'/;listen.group = nobody/listen.group = nginx/' /etc/php-fpm.d/www.conf
sed -i s'/;listen.mode = 0660/listen.mode = 0660/' /etc/php-fpm.d/www.conf
sed -i s'/user = apache/user = nginx/' /etc/php-fpm.d/www.conf
sed -i s'/group = apache/group = nginx/' /etc/php-fpm.d/www.conf
sed -i s'/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php.ini
sed -i s'/display_errors = Off/display_errors = On/' /etc/php.ini
systemctl enable php-fpm
systemctl restart php-fpm
sed -i -e '38,57d' /etc/nginx/nginx.conf
cat <<EOT > /etc/nginx/conf.d/default.conf
server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;

    root   /usr/share/nginx/html;
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ =404;
    }
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ~ .php$ {
        try_files \$uri =404;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }
}
EOT
systemctl enable nginx
systemctl restart nginx

# test db
yum install -y mariadb
while ! mysqladmin ping -h"$DB_HOST" -u"$DB_USER" -p"$DB_PASS" --silent; do
    echo "wait mysql up..."
    sleep 2s
done

rm -rf /usr/share/nginx/html/*
curl -o latest.tar.gz -L https://github.com/WordPress/WordPress/archive/5.3.tar.gz
tar -C /usr/share/nginx/html/ -xvf ./latest.tar.gz --strip 1
rm -rf latest.tar.gz

curl -s -o /usr/share/nginx/html/wp-config.php -X POST -d "db_name=$DB_NAME&db_user=$DB_USER&db_password=$DB_PASS&db_host=$DB_HOST&db_charset=utf8mb4&db_collate=utf8mb4_general_ci&table_prefix=wpym5jme_&custom_user_table=&custom_user_meta_table=&securitykeys=on&force_ssl_login=&force_ssl_admin=&fatal_error_handler=true&wp_update_php_url=&wp_siteurl=&wp_home=&wp_content_dir=&wp_content_url=&wp_plugin_dir=&wp_plugin_url=&uploads=&cookie_domain=&test_cookie=wordpress_test_cookie&logged_in_cookie=wordpress_logged_in_&secure_auth_cookie=wordpress_logged_in_&auth_cookie=wordpress_&pass_cookie=wordpresspass_&user_cookie=wordpressuser_&recovery_mode_cookie=wordpress_rec_&autosave_internal=&wp_post_revisions=5&media_trash=true&empty_trash_days=7&wp_mail_interval=86400&wp_memory_limit=128M&wp_max_memory_limit=256M&automatic_updater_disabled=false&wp_auto_update_core=minor&core_upgrade_skip_new_bundled=true&do_not_upgrade_global_tables=false&disallow_file_mods=false&disallow_file_edit=true&image_edit_overwrite=true&wp_cache=true&wp_cache_key_salt=0r4wrz04vdal6i3xpn%3A&compress_css=true&compress_scripts=true&concatenate_scripts=false&enforce_gzip=true&disable_wp_cron=false&alternate_wp_cron=false&wp_cron_lock_timeout=60&fs_method=&ftp_base=&ftp_content_dir=&ftp_plugin_dir=&ftp_pubkey=&ftp_prikey=&ftp_user=&ftp_pass=&ftp_host=&ftp_ssl=true&wpmu_plugin_dir=&wpmu_plugin_url=&muplugindir=&disallow_unfiltered_html=false&allow_unfiltered_uploads=false&magpie_cache_on=true&magpie_cache_dir=cache&magpie_cache_age=3600&magpie_debug=false&magpie_user_agent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%3B+rv%3A64.0%29+Gecko%2F20100101+Firefox%2F64.0&magpie_fetch_time_out=5&magpie_use_gzip=true&wp_allow_multisite=false&subdomain_install=false&noblogredirect=&wp_default_theme=twentytwenty&wp_http_block_external=false&wp_accessible_hosts=*.wordpress.org%2C*.github.com&fs_chmod=false&wp_proxy_host=&wp_proxy_port=&wp_proxy_username=&wp_proxy_password=&wp_proxy_bypass_hosts=&wp_debug=false&wp_debug_display=false&wp_debug_log=false&wp_debug_log_path=&script_debug=false&savequeries=false&download="  https://wp-config.pro/
cat <<EOT >> /usr/share/nginx/html/wp-config.php
define( 'WP_SITEURL', '/' );
define( 'WP_HOME', '/' );

if (isset(\$_SERVER['HTTP_X_FORWARDED_HOST'])) {
    \$_SERVER['HTTP_HOST'] = \$_SERVER['HTTP_X_FORWARDED_HOST'];
}
EOT

#chown -R www:www /data/wwwroot/default
