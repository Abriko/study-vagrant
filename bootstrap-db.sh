#!/bin/bash
echo "run bootstrap-db.sh"

yum install -y mariadb mariadb-server
systemctl enable mariadb
systemctl start mariadb

/bin/mysql_secure_installation << EOF

Y
$DB_PASS
$DB_PASS
Y
Y
Y
Y
EOF

Q1="CREATE DATABASE IF NOT EXISTS $DB_NAME;"
Q2="GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'%' IDENTIFIED BY '$DB_PASS';"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"

/usr/bin/mysql -uroot -p"$DB_PASS" -e "$SQL"
/usr/bin/mysql -u$DB_USER -p"$DB_PASS" wp < /db/wp.sql
