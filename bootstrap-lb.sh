#!/bin/bash
echo "run bootstrap-lb.sh"

yum install -y epel-release 
yum install -y nginx
sed -i -e '38,57d' /etc/nginx/nginx.conf
cat <<EOT > /etc/nginx/conf.d/default.conf
upstream wp {
    server 192.168.100.100;
}
server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;

    root   /usr/share/nginx/html;
    index index.php index.html index.htm;

    location / {
        proxy_pass http://wp;
        proxy_set_header   Host    \$host;
        proxy_set_header   X-Real-IP   \$remote_addr;
        proxy_set_header   X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_redirect off;

    }
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

}
EOT
systemctl enable nginx
systemctl restart nginx
